/*
* Large Numbers List Plugin
*
* @author Sagar Boina <sagar.sijju@gmail.com>
* @version 1.0
*/
(function () {
    CKEDITOR.plugins.add( 'largenumberslist', {
        icons: 'largenumberslist',
        init: function( editor ) {
            editor.addCommand( 'largenumberslist', new CKEDITOR.dialogCommand( 'largenumberslist' ) );
            editor.ui.addButton( 'largenumberslist', {
                label: 'Add Large Numbers List',
                command: 'largenumberslist',
                toolbar: 'insert'
     
            });
            let plugin_path = this.path;
			
			
			CKEDITOR.dialog.add('largenumberslist', function (editor) {
				return {
					title: 'Large Numbers List',
					minWidth: 400,
					minHeight: 200,
					contents: [
						{
							id: 'LargenumbersPlugin',
							label: 'Basic Settings',
							elements: [
								{
									type: 'textarea',
									id: 'largenumbers_text',
									label: 'Largenumbers List',
									setup: function( element ) {
										this.setValue( element.getText() );
									},
									commit: function( element ) {
										element.setText( this.getValue() );
									}
								}
							]
						}
					],
					onShow: function() {
						var selection = editor.getSelection();
						var element = selection.getStartElement();
						this.element = element;
						this.setupContent( this.element );						
					},
					onOk: function() {
						var txt = this.getValueOf('LargenumbersPlugin', 'largenumbers_text');
						var list_txt = '<ol class="largenumber-list">';
						
						var lines = txt.split('\n');
						
						for(var i = 0;i < lines.length;i++){
							//code here using lines[i] which will give you each line
							if(lines[i] != ''){
								list_txt += '<li>'+lines[i]+'</li>';
							}
						}
						
						list_txt += '</ol>';

						editor.insertHtml( list_txt );
					}
				}
			}); //dialog ends
    } //init ends
	}) //add ends
})()